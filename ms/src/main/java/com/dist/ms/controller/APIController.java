package com.dist.ms.controller;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description TODO
 * @Author chenzhw
 * @Date 2023/4/25 17:01
 * @Version 1.0
 */
@RestController
public class APIController {

    @RequestMapping("/say")
    public String  say(){
        return "hello word";
    }

    @RequestMapping("/rap")
    public String  rap(){
        return "rap";
    }

}
